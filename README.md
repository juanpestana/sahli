# SAHLI

Este repositorio incluye el código fuente del sistema Sahli de la Clínica Oftalmológica del Caribe.

El proyecto cuenta con varias carpetas y archivos, los cuáles se describen a continuación aquellos importantes y relevantes del desarrollo.

* Carpeta DashboardOCT contiene los archivos de configuración y despliegue del sistema de base de datos e interfaz utilizada. Para este proyecto se encuentra en ejecución una base de datos local por  SQLite.
* Carpeta App contiene las vistas que renderizan la interfaz gráfica de Sahli y los controladores utilizados para tomar la imagen, enviarla a servidor y aplicar los modelos entrenados para arrojar una predicción un un mapa de calor.
* La carpeta static contiene los archivos estáticos para el funcionamiento de la interfaz web (archivos CSS, JS e imágenes). Adicionalmente dentro de esta, se encuentra la carpeta models que incluye los modelos entrenados que son aplicados sobre las imágenes de OCT.
* La carpeta templates indica los archivos html utilizados sobre la interfaz web.

## Instalación
Para este punto se asume que el servidor o equipo a utilizar cuenta con una instalación activa de [Python](https://python.org) en su versión 3.6 o 3.8.
Además del gestor de paquetes de Python llamado [pip](https://pip.pypa.io/en/stable/) para la instalación de los recursos y librerías necesarias.

Adicionalmente se debe clonar este repositorio en el lugar que se desee dentro del equipo o servidor.

```git
git clone https://gitlab.com/juanpestana/sahli
```
Instalamos los paquetes necesarios, ingresando a la carpeta sahli y ejecutando el siguiente comando.
```bash
pip install -r requirements.txt
```

Luego de que finalice la instalación, todos los paquetes estarán instalados y listos para trabajar.

## Configuración
Dentro de la carpeta DashboardOCT, en el archivo settings.py se deben ajustar las variables de STATIC_ROOT y MEDIA_ROOT, con las ubicaciones físicas donde queremos alojar los archivos estáticos y los archivos de media (son todos aquellos cargados por el usuario).

Luego de configurar los parámetros necesarios se procede a hacer las migraciones del sitio. Esta acción llevará la estructura de datos a una base de datos, la cual es informada en las sección DATABASE del archivo settings.py. Para más información sobre esta configuración, se puede ver [en este enlace](https://docs.djangoproject.com/en/2.2/ref/settings/#databases).

## Puesta en marcha
Una vez configurado se puede ejecutar la plataforma a través del siguiente comando

```bash
python manage.py runserver 0.0.0.0:8000
```
Donde el 8000 corresponde al puerto del equipo o servidor donde queramos poner a correr la interfaz.
Luego de ejecutar este comando, su primer despliegue puede tomar tiempo debido que debe instanciar los recursos necesarios para la evaluación de las imágenes a través de TensorFlow.
