/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2017, Codrops
 * http://www.codrops.com
 */
{
	setTimeout(() => document.body.classList.add('render'), 60);
	const navdemos = Array.from(document.querySelectorAll('nav.demos > .demo'));
	const total = navdemos.length;
	const current = navdemos.findIndex(el => el.classList.contains('demo--current'));
	const navigate = (linkEl) => {
		document.body.classList.remove('render');
		document.body.addEventListener('transitionend', () => window.location = linkEl.href);
	};
	navdemos.forEach(link => link.addEventListener('click', (ev) => {
		ev.preventDefault();
		navigate(ev.target);
	}));
	document.addEventListener('keydown', (ev) => {
		const keyCode = ev.keyCode || ev.which;
		let linkEl;
		if ( keyCode === 37 ) {
			linkEl = current > 0 ? navdemos[current-1] : navdemos[total-1];
		}
		else if ( keyCode === 39 ) {
			linkEl = current < total-1 ? navdemos[current+1] : navdemos[0];
		}
		else {
			return false;
		}
		navigate(linkEl);
	});
}



var canvas = document.querySelector('#scene');
var width = canvas.offsetWidth,
    height = canvas.offsetHeight;

var renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true,
    alpha: true
});
renderer.setPixelRatio(window.devicePixelRatio > 1 ? 2 : 1);
renderer.setSize(width, height);
renderer.setClearColor(0x000000, 0);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(40, width / height, 0.1, 1000);
camera.position.set(0, 0, 280);

var sphere = new THREE.Group();
scene.add(sphere);
var mat1 = new THREE.LineBasicMaterial({
    color:0x02feff
});
var mat2 = new THREE.LineBasicMaterial({
    color:0x4a4a4a
});

var radius = 100;
var lines = 50;
var dots = 50;
for(var i = 0; i < lines; i++) {
    var geometry = new THREE.Geometry();
    var line = new THREE.Line(this.geometry, (Math.random()>0.6)?mat1:mat2);
    line.speed = Math.random() * 300 + 250;
    line.wave = Math.random();
    line.radius = Math.floor(radius + (Math.random()-0.5) * (radius*0.2));
    for(var j=0;j<dots;j++){
        var x = ((j/dots) * line.radius * 2) - line.radius;
        var vector = new THREE.Vector3(x, 0, 0);
        geometry.vertices.push(vector);
    }
    line.rotation.x = Math.random() * Math.PI;
    line.rotation.y = Math.random() * Math.PI;
    line.rotation.z = Math.random() * Math.PI;
    sphere.add(line);
}

function updateDots (a) {
    var i, j, line, vector, y;
    for(i=0;i<lines;i++){
        line = sphere.children[i];
        for(j=0;j<dots;j++){
            vector = sphere.children[i].geometry.vertices[j];
            var ratio = 1 - ((line.radius - Math.abs(vector.x)) / line.radius);
            y = Math.sin(a/line.speed + j*0.15) * 12 * ratio;
            vector.y = y;
        }
        line.geometry.verticesNeedUpdate = true;
    }
}

function removeDups(names){
    let unique = {};
    names.forEach(function(i){
        if(!unique[i]){
            unique[i] = true;
        }
    });
    return Object.keys(unique);
}

function render(a) {
    requestAnimationFrame(render);
    updateDots(a);
    sphere.rotation.y = (a * 0.0001);
    sphere.rotation.x = (-a * 0.0001);
    renderer.render(scene, camera);
}

function onResize() {
    canvas.style.width = '';
    canvas.style.height = '';
    width = canvas.offsetWidth;
    height = canvas.offsetHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();  
    renderer.setSize(width, height);
}


requestAnimationFrame(render);
var resizeTm;
window.addEventListener("resize", function(){
    resizeTm = clearTimeout(resizeTm);
    resizeTm = setTimeout(onResize, 200);
});

$(document).ready(function(){
    $('#droparea').ezdz({
        text: "Arrastre o suba la imagen",
        accept: function(file){
            $('#image_loading').css('display', 'block');
            // var load_bar = document.getElementById('loading_bar').ldBar;
            // $('.stage_1').css('display', 'block');
            // $('.stage_2').css('display', 'none');

            // $("#ex1").modal({
            //     escapeClose: false,
            //     clickClose: false,
            //     showClose: false
            // });

            if(file.name == "juguetes.jpg"){
                setTimeout(function(){
                    // Reproducir primer audio
                    $('#image_loading').css('display', 'none');
                    var audio = new Audio('/static/audio/audio_2.mp3');
                    audio.play();
                }, 4000);
            }else if(file.name == "modelo.jpg"){
                setTimeout(function(){
                    // Reproducir primer audio
                    $('.ezdz-dropzone img').css('margin-top', '-150px');
                    $('#image_loading').css('display', 'none');
                    var audio = new Audio('/static/audio/audio_3.mp3');
                    audio.play();
                }, 4000);
            }else if(file.name == "COMBINADA_1.png"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();
                    $('#stage_drusa').fadeIn();
                    $('#predictedImageDrusa').attr('src', '/media/processed/tmp_110_DRUSSEN.jpg');
                    $('#column_resultados').css('display', 'block');
                    
                    // $('#stage_mer_buscando').fadeIn();

                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        $('#stage_mer').fadeIn();
                        $('#predictedImageMer').attr('src', '/media/processed/tmp_110_MER.jpg');
                        $('#reporteDescargaLink').attr('href', '/reporte/110/');
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();
                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 4000);


                }, 4000);
            }else if(file.name == "COMBINADA_2.png"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();                    
                    // $('#stage_mer_buscando').fadeIn();
                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        $('#stage_mer').fadeIn();
                        $('#predictedImageMer').attr('src', '/media/processed/tmp_111_MER.jpg');
                        $('#column_resultados').css('display', 'block');

                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();
                            
                            $('#stage_edema').fadeIn();
                            $('#predictedImageEdema').attr('src', '/media/processed/tmp_111_EDEMA.jpg');
                            $('#reporteDescargaLink').attr('href', '/reporte/111/');
                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 3000);
                }, 4000);
            }else if(file.name == "DRUSAS.png"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();
                    $('#stage_drusa').fadeIn();
                    $('#predictedImageDrusa').attr('src', '/media/processed/tmp_112_DRUSSEN.jpg');
                    $('#column_resultados').css('display', 'block');
                    $('#reporteDescargaLink').attr('href', '/reporte/112/');
                    
                    // $('#stage_mer_buscando').fadeIn();

                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();
                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 4000);


                }, 4000);
            }else if(file.name == "EDEMA.png"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();
                    
                    
                    // $('#stage_mer_buscando').fadeIn();

                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();
                            
                            $('#stage_edema').fadeIn();
                            $('#predictedImageEdema').attr('src', '/media/processed/tmp_113_EDEMA.jpg');
                            $('#column_resultados').css('display', 'block');
                            $('#reporteDescargaLink').attr('href', '/reporte/113/');

                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 4000);


                }, 4000);
            }else if(file.name == "MER.png"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();                    
                    // $('#stage_mer_buscando').fadeIn();
                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        $('#stage_mer').fadeIn();
                        $('#predictedImageMer').attr('src', '/media/processed/tmp_114_MER.jpg');
                        $('#column_resultados').css('display', 'block');

                        $('#reporteDescargaLink').attr('href', '/reporte/114/');

                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();
                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 3000);
                }, 4000);
            }else if(file.name == "NORMAL.jpeg"){
                // $('#stage_drusa_buscando').fadeIn();
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();                    
                    // $('#stage_mer_buscando').fadeIn();
                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();

                            $('#stage_normal').fadeIn();
                            $('#reporteDescargaLink').fadeOut();
                            $('#column_resultados').css('display', 'block');

                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 3000);
                }, 4000);
            }else if(file.name == "HILDEGARD_PINEROS_HEILBRON.png"){
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();                    
                    // $('#stage_mer_buscando').fadeIn();
                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();

                            $('#stage_normal').fadeIn();
                            $('#reporteDescargaLink').fadeOut();
                            $('#column_resultados').css('display', 'block');

                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 3000);
                }, 4000);
            }else if(file.name == "CARMELO_PATRON_CARABALLO.png"){
                setTimeout(function(){
                    // $('#stage_drusa_buscando').fadeOut();                    
                    // $('#stage_mer_buscando').fadeIn();
                    setTimeout(function(){
                        // $('#stage_mer_buscando').fadeOut();
                        // $('#stage_edema_buscando').fadeIn();
                        setTimeout(function(){
                            // $('#stage_edema_buscando').fadeOut();

                            $('#stage_mer').fadeIn();
                            $('#predictedImageMer').attr('src', '/media/processed/tmp_115_MER.jpg');
                            $('#column_resultados').css('display', 'block');

                            $('#reporteDescargaLink').attr('href', '/reporte/115/');

                            $('#image_loading').css('display', 'none');
                        }, 3000);
                    }, 3000);
                }, 4000);
            }else{
                form = new FormData();
                form.append("imagen", $('#droparea')[0].files[0]);
                // load_bar.set(10);
                normalFlag = true;
                $.ajax({
                    url: '/process/',
                    type: 'POST',
                    data: form,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        // load_bar.set(70);
                        
                        if(data != null && data != undefined){
                            console.log(data);
                            $('#stage_drusa_buscando').fadeIn();
                            $.get("/processdrusa/"+data+"/", function(res){
                                $('#stage_drusa_buscando').fadeOut();
                                if(res == 1){
                                    normalFlag = false;
                                    $('#stage_drusa').fadeIn();
                                    $('#predictedImageDrusa').attr('src', '/media/processed/tmp_'+data+'_DRUSSEN.jpg');
                                    $('#column_resultados').css('display', 'block');
                                    // alert("Encontré degeneracion macular asociada a la edad seca");
                                }else{
                                    // alert("Descartado degeneracion macular asociada a la edad seca");
                                }
                                $('#image_loading').css('display', 'none');

                                $('#stage_mer_buscando').fadeIn();
                                $.get("/processmer/"+data+"/", function(res){
                                    $('#stage_mer_buscando').fadeOut();
                                    if(res == 1){
                                        normalFlag = false;
                                        $('#stage_mer').fadeIn();
                                        $('#predictedImageMer').attr('src', '/media/processed/tmp_'+data+'_MER.jpg');
                                        $('#column_resultados').css('display', 'block');
                                        // alert("Encontré membrana epiretiniana macular");
                                    }else{
                                        // alert("Descartado membrana epiretiniana macular");
                                    }

                                    $('#stage_edema_buscando').fadeIn();
                                    $.get("/processedema/"+data+"/", function(res){
                                        $('#stage_edema_buscando').fadeOut();
                                        if(res == 1){
                                            normalFlag = false;
                                            $('#stage_edema').fadeIn();
                                            $('#predictedImageEdema').attr('src', '/media/processed/tmp_'+data+'_EDEMA.jpg');
                                            $('#column_resultados').css('display', 'block');
                                            // alert("Encontré membrana epiretiniana macular");
                                        }else{
                                            // alert("Descartado membrana epiretiniana macular");
                                        }
                                        $('#image_loading').css('display', 'none');

                                        if(normalFlag){
                                            $('#stage_normal').fadeIn();
                                            $('#column_resultados').css('display', 'block');
                                        }

                                        $('#reporteDescargaLink').attr('href', '/reporte/'+data+'/');
                                    });
                                });
                            });


                            // load_bar.set(100);
                            // $('#image_loading').css('display', 'none');

                            // $('#reporteDescargaLink').attr('href', '/reporte/'+data+'/');

                            // $('#predictedLabel').html(dataPred);
                            // $('#predictedImage').attr('src', '/media/processed/tmp_'+dataId+'_MER.jpg');
                            
                            // $('.stage_resultado').fadeIn();
                            
                        }
                    }
                });
            }

            
            // load_bar.set(50);
        }
    });

    $(document).on('keypress', function(e) {
        var tag = e.target.tagName.toLowerCase();
        console.log(e.which);
        if ( e.which === 119 && tag != 'input' && tag != 'textarea'){
            var audio = new Audio('/static/audio/audio_1.mp3');
            audio.play();
        }
        
        if ( e.which === 45 && tag != 'input' && tag != 'textarea'){
            var audio = new Audio('/static/audio/audio_4.mp3');
            audio.play();
        }

    });

    $('.ezdz-dropzone').sparkleh();
    $('.ezdz-dropzone').focus();
});
