/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2017, Codrops
 * http://www.codrops.com
 */
{
	setTimeout(() => document.body.classList.add('render'), 60);
	const navdemos = Array.from(document.querySelectorAll('nav.demos > .demo'));
	const total = navdemos.length;
	const current = navdemos.findIndex(el => el.classList.contains('demo--current'));
	const navigate = (linkEl) => {
		document.body.classList.remove('render');
		document.body.addEventListener('transitionend', () => window.location = linkEl.href);
	};
	navdemos.forEach(link => link.addEventListener('click', (ev) => {
		ev.preventDefault();
		navigate(ev.target);
	}));
	document.addEventListener('keydown', (ev) => {
		const keyCode = ev.keyCode || ev.which;
		let linkEl;
		if ( keyCode === 37 ) {
			linkEl = current > 0 ? navdemos[current-1] : navdemos[total-1];
		}
		else if ( keyCode === 39 ) {
			linkEl = current < total-1 ? navdemos[current+1] : navdemos[0];
		}
		else {
			return false;
		}
		navigate(linkEl);
	});
}



var canvas = document.querySelector('#scene');
var width = canvas.offsetWidth,
    height = canvas.offsetHeight;

var renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true,
    alpha: true
});
renderer.setPixelRatio(window.devicePixelRatio > 1 ? 2 : 1);
renderer.setSize(width, height);
renderer.setClearColor(0x000000, 0);

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(40, width / height, 0.1, 1000);
camera.position.set(0, 0, 280);

var sphere = new THREE.Group();
scene.add(sphere);
var mat1 = new THREE.LineBasicMaterial({
    color:0x02feff
});
var mat2 = new THREE.LineBasicMaterial({
    color:0x4a4a4a
});

var radius = 100;
var lines = 50;
var dots = 50;
for(var i = 0; i < lines; i++) {
    var geometry = new THREE.Geometry();
    var line = new THREE.Line(this.geometry, (Math.random()>0.6)?mat1:mat2);
    line.speed = Math.random() * 300 + 250;
    line.wave = Math.random();
    line.radius = Math.floor(radius + (Math.random()-0.5) * (radius*0.2));
    for(var j=0;j<dots;j++){
        var x = ((j/dots) * line.radius * 2) - line.radius;
        var vector = new THREE.Vector3(x, 0, 0);
        geometry.vertices.push(vector);
    }
    line.rotation.x = Math.random() * Math.PI;
    line.rotation.y = Math.random() * Math.PI;
    line.rotation.z = Math.random() * Math.PI;
    sphere.add(line);
}

function updateDots (a) {
    var i, j, line, vector, y;
    for(i=0;i<lines;i++){
        line = sphere.children[i];
        for(j=0;j<dots;j++){
            vector = sphere.children[i].geometry.vertices[j];
            var ratio = 1 - ((line.radius - Math.abs(vector.x)) / line.radius);
            y = Math.sin(a/line.speed + j*0.15) * 12 * ratio;
            vector.y = y;
        }
        line.geometry.verticesNeedUpdate = true;
    }
}


function render(a) {
    requestAnimationFrame(render);
    updateDots(a);
    sphere.rotation.y = (a * 0.0001);
    sphere.rotation.x = (-a * 0.0001);
    renderer.render(scene, camera);
}

function onResize() {
    canvas.style.width = '';
    canvas.style.height = '';
    width = canvas.offsetWidth;
    height = canvas.offsetHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();  
    renderer.setSize(width, height);
}


requestAnimationFrame(render);
var resizeTm;
window.addEventListener("resize", function(){
    resizeTm = clearTimeout(resizeTm);
    resizeTm = setTimeout(onResize, 200);
});

$(document).ready(function(){
    $('#droparea').ezdz({
        text: "Arrastre o suba la imagen",
        accept: function(file){
            $('#image_loading').css('display', 'block');
            if(file.name == "juguetes.jpg"){
                setTimeout(function(){
                    // Reproducir primer audio
                    $('#image_loading').css('display', 'none');
                    var audio = new Audio('/static/audio/audio_2.mp3');
                    audio.play();
                }, 4000);
            }

            if(file.name == "modelo.jpg"){
                setTimeout(function(){
                    // Reproducir primer audio
                    $('.ezdz-dropzone img').css('margin-top', '-150px');
                    $('#image_loading').css('display', 'none');
                    var audio = new Audio('/static/audio/audio_3.mp3');
                    audio.play();
                }, 4000);
            }
        }
    });

    $(document).on('keypress', function(e) {
        var tag = e.target.tagName.toLowerCase();
        console.log(e.which);
        if ( e.which === 119 && tag != 'input' && tag != 'textarea'){
            var audio = new Audio('/static/audio/audio_1.mp3');
            audio.play();
        }
        
        if ( e.which === 45 && tag != 'input' && tag != 'textarea'){
            var audio = new Audio('/static/audio/audio_4.mp3');
            audio.play();
        }

    });

    $('.ezdz-dropzone').sparkleh();
    $('.ezdz-dropzone').focus();
});