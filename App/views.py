from django.shortcuts import render
from .models import *
from django.http import HttpResponse, JsonResponse
from UFOPredictor import UFOPredictor
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import get_template, render_to_string
from django.template import Context
from django.http import HttpResponse, FileResponse, JsonResponse
import pdfkit

Predictor = UFOPredictor.UFOPredictor()
Predictor.addModelPaths("DRUSSEN", "/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/static/models/drusen_inceptionV3.json", "/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/static/models/drusen_inceptionV3.h5", "Degeneración macular asociada a la edad seca")
Predictor.addModelPaths("MER",)
Predictor.addModelPaths("EDEMA",)

def main(request):
    return render(request, "main.html", {})

@csrf_exempt
def processData(request):
    global _oct
    if request.method == 'POST':
        imagenFile = request.FILES.get('imagen')

        imagen = ImagenesProcesadas()
        imagen.imagen = imagenFile
        imagen.save()

        # # sio.emit('process', {'imagen': imagen.imagen.url, 'imagen_pk': imagen.pk})

        # # Procesamiento aqui
        # _oct.setVars("/home/OCT/oct-dashboard-interface/{}".format(imagen.imagen.url), imagen.pk)
        # isDrusa = _oct.processDrusas()
        
        # isMer = _oct.processMER()
        
        # isAtrofia = _oct.processAtrofia()
        
        # isEdema = _oct.processEdema()
        

        # imagen.drusas = _oct.isDrusa
        # imagen.atrofia = _oct.isAtrofia
        # imagen.edema = _oct.isEdema
        # imagen.mer = _oct.isMER
        # imagen.save()
        
        # prediccionStr = ""
        # if imagen.drusas:
        #     prediccionStr += "Degeneración macular asociada a la edad seca"
        # if imagen.atrofia:
        #     prediccionStr += "Adelgazamiento coroideo"
        # if imagen.edema:
        #     prediccionStr += "Edema macular quístico"
        # if imagen.mer:
        #     prediccionStr += "Membrana epiretiniana macular"

        return HttpResponse(imagen.pk)
    return HttpResponse(0)



def evaluarDrusa(request, imagenpk):
    
    imagen = ImagenesProcesadas.objects.get(pk = imagenpk)
    Predictor.setPredictor("DRUSSEN")
    
    isDrusa = Predictor.process("/home/OCT/oct-dashboard-interface/{}".format(imagen.imagen.url), "/home/OCT/oct-dashboard-interface/media/processed/tmp_{}_{}.jpg".format(imagen.pk, "DRUSSEN"))

    imagen.drusas = isDrusa
    imagen.save()
    if isDrusa:
        return HttpResponse(1)
    else:
        return HttpResponse(0)

def evaluarMer(request, imagenpk):
    
    imagen = ImagenesProcesadas.objects.get(pk = imagenpk)
    Predictor.setPredictor("MER")
    isMER = Predictor.process("/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/{}".format(imagen.imagen.url), "/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/media/processed/tmp_{}_{}.jpg".format(imagen.pk, "DRUSSEN"))
    
    imagen.mer = isMER
    imagen.save()
    if isMER:
        return HttpResponse(1)
    else:
        return HttpResponse(0)

def evaluarEdema(request, imagenpk):
    imagen = ImagenesProcesadas.objects.get(pk = imagenpk)
    Predictor.setPredictor("EDEMA")
    isEdema = Predictor.process("/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/{}".format(imagen.imagen.url), "/Volumes/JPPN/Users/juanpestana/Ufotech/Cofca/Sahali/oct-dashboard-interface/media/processed/tmp_{}_{}.jpg".format(imagen.pk, "DRUSSEN"))
    
    imagen.edema = isEdema
    imagen.save()
    if isEdema:
        return HttpResponse(1)
    else:
        return HttpResponse(0)

@csrf_exempt
def processDataExamen(request):
    if request.method == 'POST':
        numExamen = request.POST.get("examen")
        
        if int(numExamen) == 1:
            # oct.setVars('/home/OCT/oct-dashboard-interface/static/images/drusen_1.jpeg', 1)
            # oct.process()
            # prediccion = oct.prediccion
            pass
        else:
            pass

        return HttpResponse(prediccion)
    return HttpResponse(0)

def renderTemplate(request):
    return render(request, "interface.html", {})

def renderSecondTemplate(request):
    request.session["drusa"] = False
    request.session["mer"] = False
    request.session["atrofia"] = False
    request.session["edema"] = False
    return render(request, "oct_v1.html", {})

def renderThirdTemplate(request):
    return render(request, "oct_v2.html", {})

@csrf_exempt
def reporte(request, idanalisis):
    an = ImagenesProcesadas.objects.get(pk = idanalisis)

    template = get_template("reporte.html")

    html = template.render({
        "an":an
    })
    options = {
        'page-size': 'Letter',
        'encoding': "UTF-8",
    }

    pdf = pdfkit.from_string(html, False, options)
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment;filename="reporte.pdf"'

    return response
