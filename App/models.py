from django.db import models

# Create your models here.

class ImagenesProcesadas(models.Model):
    imagen = models.FileField(upload_to="uploads", blank=True, null=True)
    imagen_procesada = models.FileField(upload_to="uploads/processed/", blank=True, null=True)

    drusas = models.BooleanField(default=False)
    mer = models.BooleanField(default=False)
    atrofia = models.BooleanField(default=False)
    edema = models.BooleanField(default=False)

    def __str__(self):
        return "Imagen procesada"