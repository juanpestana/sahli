"""DashboardOCT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from App import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.renderSecondTemplate),
    path('process/', views.processData),
    path('processdrusa/<int:imagenpk>/', views.evaluarDrusa),
    path('processmer/<int:imagenpk>/', views.evaluarMer),
    path('processedema/<int:imagenpk>/', views.evaluarEdema),

    path('oct/', views.renderTemplate),
    path('procesar/', views.processDataExamen),
    path('oct2/', views.renderSecondTemplate),
    path('oct3/', views.renderThirdTemplate),
    path('reporte/<int:idanalisis>/', views.reporte),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
